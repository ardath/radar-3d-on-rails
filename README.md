# Radar 3D on Rails
Aplikacja prezentująca pomiary radarowe z użyciem biblioteki CesiumJS

Wymaga zaimportowania danych z plików CSV.

Wyświetla tylko mały wybrany fragment danych, dla większej ilości punktów
zawieszał się mój komputer co utrudniało testowanie...
Dlatego ograniczyłem dane do bardzo małej domeny i wyświetlam tylko najniższe 3
warstwy skanu radarowego, dodatkowo wybierając wartości opadu mniejsze od 35 [mm].
(filtracja jest w skrypcie insert-csv.rb)

# Wymagania
* Ruby 2.3+
* Bash 4+
* sqlite

# Instalacja

```bash
  gem install bundler

  # poniższe polecenia wykonujemy w katalogu projektu:

  bundle install # instalacja RoR i wymaganych bibliotek

  bundle exec rake db:create db:migrate # przygotowanie bazy danych

  bundle exec rails server # uruchomienie serwera aplikacji ruby on rails

  ./batch-insert-csv.sh # import danych z plików CSV (pliki są w katalogu data/)

```

W przeglądarce wchodzimy na stronę localhost:3000

W Menu w prawym górnym rogu pod ikonką "?" jest pomoc w poruszaniu po viewporcie.

Ikonka Home przenosi do obszaru dla którego zostały zaimportowane dane.

Klikając na punkty możemy sprawdzić wartość pomiaru i współrzędne.

Widok aplikacji przedstawia screenshot:

![Screenshot](screenshot.png "screenshot")

# Dodatkowe wykorzystane skrypty programy do przetworzenia danych

* prepare-input-data.sh
  Skrypt przetwarza dane z formatu Radarowego w jakim je dostałem oryginalnie.
  Konwertuje ESRI ASCII grid do pliku kolumnowego XYZ z wykorzystaniem pakietu
  GDAL.

  Następnie filtruje z pomocą awk dane nieprawidłowe i robi reprojekcję odwrotną
  z PUWG92 do współrzędnych geograficznych.

  Dane wyjściowe są w formacie CSV który jest importowany do bazy danych
  aplikacji przy pomocy skryptu bath-insert-csv.sh.
