#!/usr/bin/bash

for asc_file in data/asc/*.asc; do
  echo Processing file: $asc_file
  xyz_file=${asc_file//.asc}.xyz
  filtered_xyz_file=$xyz_file.filtered
  asc_basename=$(basename $asc_file)
  csv_file=data/${asc_basename//.asc}.csv

  echo Translating ESRI ASCII grid to XYZ file
  gdal_translate -of xyz $asc_file $xyz_file

  echo Filtering values smaller than 0 and greater than 9999 \(errors and NODATA values\)
  awk '{ FS=" "  } $3 > 0 && $3 < 9999 { print $0  }' $xyz_file > $filtered_xyz_file

  echo Reprojecting from PUWG92 projection to geographical coordinates
  proj -I -f %.3f  +init=epsg:2180 $filtered_xyz_file > $csv_file
  echo Finished processing file: $asc_file
  echo
done
