class AddHeightToRadarMeasurement < ActiveRecord::Migration[5.0]
  def change
    add_column :radar_measurements, :height, :integer
  end
end
