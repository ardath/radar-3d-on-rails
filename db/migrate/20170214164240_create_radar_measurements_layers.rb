class CreateRadarMeasurementsLayers < ActiveRecord::Migration[5.0]
  def change
    create_table :radar_measurements_layers do |t|
      t.references :point, foreign_key: true
      t.datetime :date
      t.integer :height_asl

      t.timestamps
    end
  end
end
