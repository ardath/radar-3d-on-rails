class CreateRadarMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :radar_measurements do |t|
      t.float :lat
      t.float :long
      t.float :value

      t.timestamps
    end
  end
end
