#!/usr/bin/env bash

for csv_file in data/*.csv; do
    csv_file_base=$(basename $csv_file)
    height_km=${csv_file_base:3:1}
    # only import first layers of data
    if [[ $height_km > 2 ]]; then
        continue
    fi
    echo Inserting CSV data from file $csv_file to database
    ./insert-csv.rb $csv_file
done
