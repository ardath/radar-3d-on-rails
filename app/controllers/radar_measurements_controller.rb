class RadarMeasurementsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @radar_measurements = RadarMeasurement.all
    # @radar_measurements = RadarMeasurement.where("height < 300")
    render json: @radar_measurements
  end

  def create
    @radar_measurement = RadarMeasurement.new(radar_measurement_params)
    if @radar_measurement.save
      render json: @radar_measurement
    else
      render json: @radar_measurement.errors, status: :unprocessable_entity
    end
  end

  private

  def radar_measurement_params
    params.require(:radar_measurement).permit(:lat, :long, :value, :height)
  end
end
