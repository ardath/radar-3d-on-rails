#!/usr/bin/env ruby

require 'rest-client'

csv_file = ARGV[0]
lines = File.open(csv_file) { |f| f.readlines }

bn = File.basename(csv_file)

height_100m, height_km = bn.match(/(\d\d)-(\d)/).captures.map(&:to_f)
height_m = (height_km + 0.1 * height_100m) * 1000

measurements = lines.map do |line|
  line.split.map(&:to_f)
end

measurements.each do |measurement|
  longitude, latitude, value = measurement

  if latitude.nil? or longitude.nil? or value.nil? or height_m.nil?
    puts "Error while importing measurement %s" % measurement
    next
  end
  new = {radar_measurement: {lat: latitude, long: longitude, value: value, height: height_m}}
  # only import small amount of data to show
  if latitude > 50 and latitude < 50.1 and longitude > 15.9 and longitude < 16.00 and value <= 35
    puts "Importing radar_measurement to database..."
    puts RestClient.post("http://localhost:3000/radar_measurements", new)
  end
end
