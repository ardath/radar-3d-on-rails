Rails.application.routes.draw do
  resources :radar_measurements_layers
  resources :radar_measurements
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'main_page#index'
end
